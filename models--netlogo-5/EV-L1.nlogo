;;;
;;; D. Thompson: "EV on a simple fitness landscape"
;;;
;;;
;;; roughly following K. DeJong's simple fitness landscape L1 (1.3)
;;;
;;; chromosome:      a single real number between -100 and 100
;;; mutation delta:  1.0
;;; births/deaths:   no death nor birth; just mutation of existing chromosomes
;;; population size: 10
;;; selection:       each chromosome mutated, then immediate comparison w/parent -> fittest chromosome survives
;;; fitness:         50 - chromosome ^ 2
;;;

;;;
;;; visual coding during selection:
;;;   yellow:                  new chromosome
;;;   large size, non-yellow:  deletion candidate


turtles-own [
  chromosome           ;; single real number
  fitness
  id                   ;; FIXME: not unique, cloned at HATCH, not WHO
                       ;; - unique at start of a run with respect to a new population of turtles [ range is 0 to population size - 1 ]
]

globals [
  default-turtle-shape

  winner         ;; turtle that currently has the best solution

  chromosome-pos-max  ;; minimum value a chromosome can have at a given position
  chromosome-pos-min  ;; maximum value a chromosome can have at a given position

  practical-fitness-min
  fitness-range  ;; range of fitness values
  max-fitness    ;; maximum possible fitness value
  min-fitness    ;; minimum possible fitness value

  births
  ;; max-births defined w/slider
  n              ;; dummy until learn to use netlogo better

  ;;visualize-selection ; defined by gui
]

to setup
  ;; (for this model to work with NetLogo's new plotting features,
  ;; __clear-all-and-reset-ticks should be replaced with clear-all at
  ;; the beginning of your setup procedure and reset-ticks at the end
  ;; of the procedure.)
  __clear-all-and-reset-ticks
  set-other-defaults
  setup-plotting

  create-turtle-population
  initially-place-turtles

  update-display
  do-plotting

  setup-landscape
end

to go
  ;; EV stops at 1000 'births'
  if births >= max-births [ stop ]
	step
end

;;;
;;; EV algorithm
;;;
;;; - choose a parent randomly
;;; - produce single 'offspring' from parent: make identical copy of parent, then mutate
;;; - compute offspring fitness
;;; - select member of population to die: randomly select candidate from current population
;;;     -> keep either candidate or offspring depending on fitness
;;;     -> if offspring fitness is equal to that of candidate, retain candidate (original population m
to step
  let new-who birth
  let deletion-candidate-who [ who ] of one-of turtles
  print new-who
  print deletion-candidate-who
  ;; visually highlight turtles duking it out...
  if visualize-selection
    [
      ask turtle new-who
      [ set color 46 ; bright yellow
        set size 4 ]
      ask turtle deletion-candidate-who
      [ ;;set color 46
        set size 4 ]
      print "HEY!"
      wait 2
    ]
  let alive delete-one new-who deletion-candidate-who
  ;; visually unhighlight
  if visualize-selection
    [ wait 0.5  ;; let the survivor linger for a period of time for observer
      if turtle alive != nobody
     [ ask turtle alive
       [ set size 1
         color-using-fitness
           ] ] ]
  update-display
  tick
  do-plotting
end

to update-display
  set winner max-one-of turtles [fitness]
  ;; 'traces'
  ;;ask turtles [ stamp
    ;;forward 0.5 ]

  ;; move to position on 'fitness landscape' (corresponding patch)
  ask turtles [ place-using-fitness ]
end

to initially-place-turtles
  ask turtles [
    setxy 0 id
    facexy 100 id
    forward 1 ]
end

;; set default values for GUI values
to defaults
  set max-births 200 ;; slider
  set population-size 10  ;; slider
  ;; in L1 mutation occurs by default at every 'birth'
  set mutation-rate 100 ;(allow user to vary)
end

to set-other-defaults
  set-fitness-variables

  ;;create-turtles 1
  ;;ask one-of turtles [ set default-turtle-shape shape ]
  ;;ask turtles [die]
  ;;clear-drawing ;; remove turtle marks

  set chromosome-pos-max 100
  set chromosome-pos-min -100
  set n 0
  set births 0
end


;; ==== Landscape visualization

;; visualize fitness using colored patches
;; - position of patch corresponds to chromosome value
;; - color of patch corresponds to fitness

to setup-landscape
 ;; use 94 to 99 as dark to light blue range
  let color-min 92
  let color-max 99
  let color-range ( color-max - color-min )
   ;; one-dimensional: use pycor=0
  ;; pxcor range: use defaults (-16 to 16)
  let pxcor-range ( max-pxcor - min-pxcor )
  print "pxcor-range"
  print pxcor-range
  let x min-pxcor ;; current patch x coordinate
  repeat (1 + pxcor-range )
  [
    let patch-fitness fitness-for-chromosome patch-value->chromosome-position-value x
    ask patch x 0
    [ set pcolor ( color-min + ( ( patch-fitness - min-fitness ) / fitness-range ) * color-range ) ]
    set x ( 1 + x )
  ]
  ;; diffuse?
end

;; given a chromosome value, return corresponding patch value
to-report chromosome-position-value->patch-value [ some-value ]
  ;; map chromosome value range to pxcor range
  let chr-value-range ( chromosome-pos-max - chromosome-pos-min )
  let prange ( max-pxcor - min-pxcor )
  report ( (prange * ( ( some-value - chromosome-pos-min ) / chr-value-range )) + min-pxcor )
end

to-report patch-value->chromosome-position-value  [ some-value ]
  ;; map patch position coordinate to chromosome value
  let chr-value-range ( chromosome-pos-max - chromosome-pos-min )
  ;;show chr-value-range
  let pxcor-range ( max-pxcor - min-pxcor )
  ;;show pxcor-range
  report ( (chr-value-range * ( ( some-value + min-pxcor ) / pxcor-range )) + chromosome-pos-min )
end

;; ==== Turtles

to create-turtle-population
 create-turtles population-size [
    set chromosome 100 - random 200
    calculate-fitness
    set id n
    set n ( 1 + n )
    set color 9.999
  ]
end

;; color turtle using fitness value
to color-using-fitness-netlogo  ;; turtle procedure
 ;; use 11 to 15 (dull red to 'sharp' red) range
 ;; - alternatively could use RGB with shift from green to red
  let color-min 13
  let color-max 15.5
  let color-range ( color-max - color-min )
  set color ifelse-value ( fitness < practical-fitness-min )
     [ color-min ]
     [ ((
         ( ( fitness - practical-fitness-min ) / ( max-fitness - practical-fitness-min ) )
         * color-range )
         + color-min )
      ]
end

to color-using-fitness ;; -rgb
  ;; [ 255 50 50 is bright pink-ish red ]
  ;; distribute 205 between R and G channels
  let color-range 205
  ;; 0 to 1 value
  let fitness-index
    ifelse-value ( fitness >= practical-fitness-min )
      [ ( fitness - practical-fitness-min ) / ( max-fitness - practical-fitness-min ) ]
      [ 0 ]
  let red-color ( color-range * fitness-index )
  let green-color ( color-range - red-color )
  set color ( list red-color green-color 100 )
end

;; place turtle at x coordinate corresponding to fitness value
to place-using-fitness
      set xcor chromosome-position-value->patch-value chromosome
end

;; a turtle procedure
to move-to-unoccupied-y
  let ycors [ ycor ] of turtles
  let offset 1
  let new-ycor 1
  let sanity-cap 0
  while [ (sanity-cap < 10) and (member? new-ycor ycors) ]
  [
    set offset (random (max-pycor - (min-pycor + 1)) )
    set new-ycor (min-pycor + offset + 1)
    set sanity-cap (1 + sanity-cap)
  ]
  set ycor new-ycor
end


;; ===== Fitness

;; convenience variables
to set-fitness-variables
  set min-fitness ( 50 - 100 * 100 )
  set max-fitness 50
  set fitness-range ( max-fitness - min-fitness )
  ;; if a fitness range has unwieldy extremes...this lets us focus on region close to maximal fitness
  set practical-fitness-min -2000 ;;( max-fitness / 10 )
end

to calculate-fitness       ;; turtle procedure
  ;; For DeJong's L1 landscape fitness is (50-x^2)
  set fitness fitness-for-chromosome chromosome ;; ( 50 - chromosome ^ 2 )
  ;; aesthetics...
  color-using-fitness
end

to-report fitness-for-chromosome [ some-chromosome ]
  report ( 50 - some-chromosome ^ 2 )
end

;;
;; in L1
;; - choose parent (randomly using uniform probability distribution over population)
;; - produce single offspring from parent (probabilistic mutation of parent)
;; - compute fitness of offspring
;; - select member of population to die: randomly select candidate from population
;;        -> keep candidate or offspring depending on which has higher fitness
;;

;; hatch a turtle and return its 'who' value
to-report birth
  let parent one-of turtles
  let offspring-who 0
  let undeleted 0
  ask parent
  [ 
    hatch 1
    [
      move-to-unoccupied-y
      mutate-turtle
      calculate-fitness
      set offspring-who who
      ;; show who -> offspring produced by HATCH have different ID value
      place-using-fitness ;; put turtle at physical position corresponding to fitness

      ;; mark turtle  ;; FIXME: this doesn't seem to happen in real-time - why not?
      ;;let old-color color
      ;;set color white
      ;;pen-down
      ;;forward 5
      ;;back 5
      ;;set color old-color
      ]]
   report offspring-who
end

;; return who value of live turtle
to-report delete-one [ who-id1 who-id2 ]
   let fitness1 [ fitness ] of turtle who-id1
   let fitness2 [ fitness ] of turtle who-id2
  ;; compare fitness values and keep winner in new population
  set births ( 1 + births )
  ifelse ( fitness1 > fitness2 )
    [ ask turtle who-id2 [ die ]
      report who-id1 ]
    [ ask turtle who-id1 [ die ]
      report who-id2 ]
end

;; ===== Mutations

;; This procedure causes random mutation(s) to occur in a chromosome
;; The associated probability is controlled by the MUTATION-RATE slider.

to-report mutate-chromosome [ some-chromosome ] ;; mutate chromosome
  report
     ifelse-value (random-float 100.0 < mutation-rate)
      [ ifelse-value (random-float 10 < 5 )
           [ some-chromosome - 1 ]
           [ some-chromosome + 1 ] ]
      [ some-chromosome ]
end

to mutate-turtle   ;; turtle procedure
  set chromosome
      ifelse-value (random-float 10 < 5 )
           [ chromosome - 1 ]
           [ chromosome + 1 ]
end

;; ====== Plotting

to setup-plotting
  ;; x interval per plot event:
  ;; 1: plot with each birth
  let x-pen-interval 1 ;; plot at each generation: generation-size
  set-current-plot "Fitness Plot"
  set-current-plot-pen "avg"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "best"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "worst"
  set-plot-pen-interval x-pen-interval

  set-current-plot "Fitness Plot (max closeup)"
  set-current-plot-pen "avg"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "best"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "worst"
  set-plot-pen-interval x-pen-interval
end

to do-plotting
  let fitness-list [fitness] of turtles
  let best-fitness max fitness-list
  let avg-fitness mean fitness-list
  let worst-fitness min fitness-list
  set-current-plot "Fitness Plot"
  set-current-plot-pen "avg"
  plot avg-fitness
  set-current-plot-pen "best"
  plot best-fitness
  set-current-plot-pen "worst"
  plot worst-fitness
  set-current-plot "Fitness Plot (max closeup)"
  set-current-plot-pen "avg"
  plot avg-fitness
  set-current-plot-pen "best"
  plot best-fitness
  set-current-plot-pen "worst"
  plot worst-fitness
end

@#$#@#$#@
GRAPHICS-WINDOW
18
10
548
134
-1
-1
5.2
1
10
1
1
1
0
1
1
1
0
99
0
17
0
0
1
ticks
30.0

BUTTON
106
174
191
207
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
18
134
105
168
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
18
214
190
247
population-size
population-size
5
200
10
5
1
NIL
HORIZONTAL

PLOT
220
133
550
283
Fitness Plot
births
raw fitness
0.0
20.0
-100.0
60.0
true
true
"" ""
PENS
"best" 1.0 0 -2674135 true "" ""
"avg" 1.0 0 -10899396 true "" ""
"worst" 1.0 0 -13345367 true "" ""

BUTTON
18
174
103
207
defaults
defaults
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
19
253
191
286
mutation-rate
mutation-rate
0
100
100
0.1
1
NIL
HORIZONTAL

SLIDER
18
291
190
324
max-births
max-births
0
2000
990
10
1
NIL
HORIZONTAL

PLOT
550
132
884
283
Fitness Plot (max closeup)
births
raw fitness
0.0
200.0
-300.0
60.0
false
true
"" ""
PENS
"avg" 1.0 0 -10899396 true "" ""
"best" 1.0 0 -2674135 true "" ""
"worst" 1.0 0 -13345367 true "" ""

MONITOR
220
301
371
350
sample chromosome
[chromosome] of one-of turtles
17
1
12

SWITCH
17
347
196
380
visualize-selection
visualize-selection
0
1
-1000

MONITOR
390
305
518
350
maximum fitness
max [ fitness ] of turtles
17
1
11

BUTTON
118
136
182
170
step
step
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## HOW TO USE IT

Press the SETUP button to create an initial population of organisms.

Press the STEP button to proceed one time through the algorithm steps, stopping at the point where the parent generation and the offspring generation can both be visualized (see VISUALIZE-P for more on visualization).

Press the GO button to cycle repeatedly through the algorithm.

Each organism is visualized as a 'turtle'. The position of the turtle on the 'fitness landscape' corresponds to the fitness of the turtle (lighter shades of blue on the fitness landscape correspond to higher fitness values).

The "Fitness Plot" depicts the highest, average, and minimum fitness values of the organisms as the model progresses.

The MUTATION-RATE slider controls the percent chance of mutation.

The POPULATION-SIZE slider controls the number of organisms present in the population.

If VISUALIZE-P is true, each turtle leaves a 'trace' between itself and its offspring as it produces one or more child organisms. Traces fade to the background color as time passes. Thus, one can visualize the consequence of each mutation event (modifying the child genome relative to the parent genome) as it occurs. Additionally, if VISUALIZE-P is true, at the time of generation of a filial population, organisms in the previous generation (parents) are visually accentuated (in a lighter shade of blue) and the organisms in the new generation (children) are visually accentuated (organism size is larger and organism shape is a 'turtle' shape). The corresponding fitness values are shown.

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
