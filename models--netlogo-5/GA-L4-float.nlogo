;;;
;;; D. Thompson: "GA on L4": roughly following K. DeJong's fitness landscape L4 (???)
;;;

;;;
;;; the 'master' code is 'GA-dejong-L2a-float-encoding.nlogo'
;;;

;;;
;;; parameters to change for altering fitness are marked with
;;;         ;; FITNESS
;;; parameters to change for altering chromosome encoding, length, etc.... are marked with:
;;;         ;; CHROMOSOME
;;; parameters to change for altering representation of fitness landscape are marked with:
;;;         ;; LANDSCAPE
;;; parameters to change for altering the selection algorithm are marked with:
;;;         ;; SELECTION
;;;

;;;
;;; chromosome:
;;;   L4 float encoding: two real numbers (range -5.5 to 4.5)
;;;   -> chromosome is composed of two floating-point numbers
;;; mutation operator: Gaussian delta
;;; population size: 10
;;; time limit: 1000 'births'
;;;

patches-own [
  path  ;; 0 if patch isn't part of a path, 1 if patch is part of a path
  ]

links-own [
  link-generation      ;; integer corresponding to most recent generation associated with the link
  age  ;; integer associated with 'age' of link (starts at 0 and increments by 1 with generation)
  ]

turtles-own [
  chromosome           ;; pair of real numbers
  deleted-p            ;; for tracing -> which turtles to fade
  fitness
  generation           ;; integer representing which generation turtle is a member of
  id ]

globals [
  ;; selection algorithm variables
  survivor       ;; turtle that most recently survived a fitness battle; set to -1 if n/a
  new-birth-who      ;; who value of turtle most recently born
  deletion-candidate   ;; turtle ready for fitness battle with new-birth
  offspring-count
  just-setup-p

  ;; chromosome variables
  chromosome-pos-max
  chromosome-pos-min
  chromosome-length
  residue-range
  word-length
  word-min
  word-max
  word-range

  ;; fitness variables
  practical-fitness-min
  fitness-range  ;; range of fitness values
  max-fitness    ;; maximum possible fitness value
  min-fitness    ;; minimum possible fitness value
  max-observed-fitness  ;; maximum fitness value achieved at any point during the run

  ;; max-births ;; slider
  births	    ;; count of births in current run
  generations  ;; count of generations in current run

  ;; RESIDUE-MUTATION-RATE: probability of a position in a chromosome sustaining a mutation
  ;; directly after 'birth' of the corresponding organism

  ;; development sandbox
  test-list
]

to setup
  ;; (for this model to work with NetLogo's new plotting features,
  ;; __clear-all-and-reset-ticks should be replaced with clear-all at
  ;; the beginning of your setup procedure and reset-ticks at the end
  ;; of the procedure.)
  __clear-all-and-reset-ticks

  ;; flag indicating setup was just called
  set just-setup-p true

  set generations 0
  set births 0
  ;; count of children created so far in current generation
  set offspring-count 0
  fixed-defaults
  setup-plotting
  setup-landscape
  create-turtle-population
  calculate-fitness-of-turtles
  color-turtles-using-fitness
  place-turtles-using-fitness
  update-display  ;; place turtles by fitness and color patches correspondingly
  ask turtles [ pen-up ]
  do-plotting
end

to defaults
  fixed-defaults
  gui-defaults
end

;; parameters end-user can define (via gui) to defaults
to gui-defaults
  set max-births 2000        ;; reset slider
  set population-size 10    ;; reset slider value
  set visualize-p true    ;; reset switch
  set residue-mutation-rate 100
  ;; mut-delta -- gui slider controlling the 'mutation delta' value
  set mut-delta 1      ;; same for bit and float encodings
end

;; set parameters not accessible by end-user to default values
to fixed-defaults
  define-fitness-landscape-parameters
  fixed-chromosome-defaults
  fixed-fitness-defaults
  ;; algorithm defaults
  set survivor -1
end

;;
;; ==== Runtime
;;
to go
  set generations 0
  loop
  [ if births >= max-births [ stop ]
    ;; SELECTION
    ;; GA:
    step-ga  ; this is a per-generation step
    ;; EV: create-next-generation-ev         ; STEP-EV is a per-birth step so we wrap it
   ]
end

to step
  ;; SELECTION
  step-ga
end

;;;
;;; GA algorithm
;;;
;;; - an initial population of POPULATION-SIZE individuals is generated
;;; - repeat until an arbitrary criterion is fulfilled:
;;;  - compute fitness of each member of the population
;;;  - select POPULATION-SIZE offspring by repeatedly
;;;    - selecting an individual from the original population where the probability of selection of the ith member of the original population is proportional to the fitness of the ith member of the original population
;;;    - subject each offspring to recombination/mutation events
;;;  - eliminate all parents (all members of the original population) so that only offspring survive
;;;

;; the GA algorithm is ordered so that pause is possible at point where new generation and
;; old generation can be visualized
to step-ga
    ;; FIXME: use VISUALLY-UNHIGHLIGHT agentset
    ;; 'kill' turtles - not visualized and not considered further except in context of links
    ask turtles with [ generation = ( generations - 1 ) ]
    [ set deleted-p true
      invisible ]
    update-display
    tick  ; ticks are per generation
    do-plotting
    create-next-generation-ga            ; make next population, update fitness values and colors
    bump-generations ;; set generations ( 1 + generations )

    if visualize-p [ visually-highlight-agentset ]
end

;; move turtles to appropriate positions
;; modify patches if necessary
to update-display
  ;; horizontal 'traces':
  ;; ask turtles [ stamp          forward 0.7 ]
  ask turtles
  [ ;; visualize parent-> descendant lineage
    ;;place-using-fitness  ;; done in CREATE-NEXT-GENERATION
    let turtle-color color
    ;;ask patch xcor ycor
    ;;[ set path 1
    ;;  set pcolor turtle-color ]
    ]
   fade-traces
end

;; assumes groups of interest are turtles at generations and generations - 1
to visually-highlight-agentset
  ask turtles with [ generation = generations ]
  [ set size 4
    set label precision fitness 1 ]
  ask turtles with [ generation = ( generations - 1 ) ]
  [ set size 4
    set color [ 0 180 180 ]
    set label precision fitness 1
    ]
end

;; turtle method
to invisible
  set size 0
  set label ""
end

to visually-highlight-turtles [ turtle1 turtle2 ]
  ask turtle1
  [ set size 5
    set label precision fitness 1
    ;; color?
    ]
  ask turtle2
  [ set size 5
    ;; set color orange ;[ 0 180 180 ]
    set label precision fitness 1
    ]
end

;; turtle method
to visually-unhighlight
  set shape "default"
  set size 1
  set label ""
  color-using-fitness
end

to bump-generations
  set generations ( 1 + generations )
  bump-trace-ages
end

;;
;; ===== Selection
;;
to create-next-generation-ga
  ;; OLD-GENERATION: agentset by selecting all currently existing turtles
  let old-generation
    ;;turtles with [true]
    turtles with [ generation = generations ]
  ; clone members of previous generation based on their fitness values
  ; where probability of selection p(selection) proportional to fitness of individual
  let local-offspring-count 0
  let new-generation ( 1 + generations )
  loop
  [ let fitness-cutoff ( random-float 1.0 * max-fitness )
    ;; choose from old population
    let random-turtle one-of old-generation with [ fitness >= fitness-cutoff ]
    if is-agent? random-turtle
      [ let random-turtle-who [ who ] of random-turtle
        ;; FIXME: should use BIRTH
        ask random-turtle
        [ hatch 1
          [ pen-up
            set generation new-generation
            mutate-turtle
            calculate-fitness-of-turtle  ;; doesn't interfere w/parent selection algorithm since agentset used
            color-using-fitness
            place-using-fitness
            let new-color color
            ;; trace from current turtle to turtle RANDOM-TURTLE-WHO
            if visualize-p [ create-trace random-turtle-who new-generation new-color ]
            ]
         set local-offspring-count ( 1 + local-offspring-count ) ]
         set births ( 1 + births )
         ]
    if ( local-offspring-count >= population-size )
    [ 
      stop ]
  ]
end

;;
;; ==== Landscape visualization
;;

;; visualize fitness using colored patches
;; - position on landscape (position of patch) corresponds to chromosome value
;; - color of patch corresponds to fitness

to define-fitness-landscape-parameters
  ;;
  ;; L2a
  ;;
  ;;set fitness-eqn "x1 ^ 2 + x2 ^ 2"  ;; string describing equation with variables x1, x2, ...
  ;;set ranges [ -5.5 4.5 -5.5 4.5 ]   ;; list with min/max pairs for each variable x1, x2, ...
end

;;
;; setup two-dimensional landscape
;;

;; variables below are set on the GUI model/world settings:
;;   max-pxcor -- netlogo restricts this to only assume positive integer values
;;   min-pxcor -- netlogo fixes that at 0
;;   max-pycor -- netlogo restricts this to only assume positive integer values
;;   min-pycor -- netlogo fixes this at 0

to setup-landscape
  ;; one-dimensional: use pycor-range of 1 ? ( -> fixed pycor = 0 )
  let pycor-range ( max-pycor - min-pycor )
  let pxcor-range ( max-pxcor - min-pxcor )
  let y min-pycor ;; current patch y coordinate
  let x min-pxcor ;; current patch x coordinate
  repeat ( 1 + pycor-range )
  [
    ;;let patch-value2 patch-y->residue y
    repeat ( 1 + pxcor-range )
    [
      color-patch-using-fitness x y
      set x ( 1 + x )
    ]
    set x min-pxcor
    set y ( 1 + y )
  ]
  ;; diffuse?
end

;; color patch at coordinates X Y using the corresponding fitness value
to color-patch-using-fitness [ x y ]
  ;; dark blue -> light blue
  let color-min 90.5
  let color-max 99.5
  let color-range ( color-max - color-min )
  ;; get corresponding chromosome subsequences
  ;; - since netlogo constrains patch coordinates to integers in range 0 to max-pxcor/pycor, we can't map patch coordinate directly to chromosome value for chromosomes such as the L4 chromosomes (which assume floating point values in the range -5.5 to 4.5)
  let value1 patch-x->residues x
  let value2 patch-y->residues y

  let patch-fitness fitness-of-chromosome ( sentence value1 value2 )
  ask patch x y
  [ set pcolor ( color-min +
      ( ( patch-fitness - min-fitness ) / fitness-range ) * color-range ) ]
end

to color-patch-using-fitness-test
  color-patch-using-fitness 0 0
end

;; return residue values (a set of chromosome values)
;; corresponding to patch x coordinate SOME-VALUE
to-report patch-x->residues  [ some-value ]
  ;; LANDSCAPE, CHROMOSOME
  ;; bit encoding: report patch-x->bit some-value
  ;; float encoding:
  report patch-x->float some-value
end

to patch-x->residues-test
  type "patch-x->residues test with 0: " print patch-x->residues 0
  type "patch-x->residues test with max-pxcor: " print patch-x->residues max-pxcor
end

to-report patch-y->residues  [ some-value ]
  ;; LANDSCAPE, CHROMOSOME
  ;; bit encoding: report patch-y->bit some-value
  ;; float encoding:
  report patch-y->float some-value
end

;; return floating point value associated with chromosome word corresponding to patch x coordinate
to-report patch-x->float [ some-value ]
  let pxcor-range ( max-pxcor - min-pxcor )
  ;;show pxcor-range
  report ( ( word-range * ( ( some-value + min-pxcor ) / pxcor-range ))
    + word-min )
end

to-report patch-y->float  [ some-value ]
  ;; map patch position coordinate to chromosome value
  ;;show chr-value-rang
  let pycor-range ( max-pycor - min-pycor )
  ;;show pxcor-range
  report ( ( word-range * ( ( some-value + min-pycor ) / pycor-range ))
    + word-min )
end

;; map a set of residues (a chromosome 'word') to a patch x coordinate
to-report residues->patch-x [ some-word ]
  report
    ;; CHROMOSOME
    float->patch-x item 0 some-word
    ;; bit->patch-x some-word
end

to-report residues->patch-y [ some-word ]
  report
    ;; CHROMOSOME
    float->patch-y item 0 some-word
    ;;bit->patch-y some-word
end

;; map floating point value associated with a chromosome word to a patch X coordinate
to-report float->patch-x [ some-value ]
  let pxrange ( max-pxcor - min-pxcor )
  report ( (pxrange * ( ( some-value - word-min ) / word-range )) + min-pxcor )
end

to-report float->patch-y [ some-value ]
  let pyrange ( max-pycor - min-pycor )
  report ( (pyrange * ( ( some-value - word-min ) / word-range )) + min-pycor )
end


;;
;; ==== Turtles
;;

;; hatch a turtle and return its 'who' value
to-report birth [ parent ]
  let child-who -1
  ask parent
  [ hatch 1
    [ mutate-turtle
      calculate-fitness-of-turtle
      place-using-fitness     ;; put turtle at physical position corresponding to fitness
      set child-who who
      set deleted-p false
      set shape "turtle"
     ]
   ]
   report child-who
end

to create-turtle-population
  let n 0
  create-turtles population-size [
    ;; CHROMOSOME
    ;; bit encoding: create-new-chromosome-bit
    ;; float encoding:
    create-new-chromosome-float
    set id n
    set n ( 1 + n )
    set generation 0
    pen-up
    set deleted-p false
  ]
end

to initially-place-turtles
  ask turtles
  [ setxy 0 id
    facexy 100 id
    forward 1 ]
end

to color-turtles-using-fitness
  ask turtles [ color-using-fitness ]
end

to place-turtles-using-fitness
  ask turtles [ place-using-fitness ]
end

;; place turtle at coordinates corresponding to fitness value
to place-using-fitness
  let x residues->patch-x chromosome-word chromosome 1
  let y residues->patch-y chromosome-word chromosome 2
  ;; setting new xcor and ycor separately leaves a right-angled trace
  ;; instead of the line directly connecting the two points
  setxy x y
end

;;
;; ==== Traces (trajectories)
;;

;; - each trace has an associated age, an integer value set to 0 at the time of birth and
;;   which is bumped up with each generation

;; turtle method - create trace from current turtle to turtle with who value turtle-who2
;; - assign 'generation' value SOME-GENERATION to the link object
to create-trace [ turtle-who2 some-generation some-color]
  ;; CREATE-TRACE-LINK
   create-link-with turtle turtle-who2    ; undirected link (no arrow)
   ask link turtle-who2 who
   [ set link-generation some-generation
     ; link colors must be RGB
     let rgb-color ifelse-value ( is-list? some-color ) [ some-color ] [ extract-rgb some-color ]
     set color rgb-color
     set age 0 ]
end

;; fade all traces
to fade-traces
  let fade-percentage 0.5    ; 0 to 100
  fade-links fade-percentage
  ;;fade-patches fade-percentage
end

to bump-trace-ages
  ;; BUMP-LINK-AGES
  ask links [ set age ( 1 + age ) ]
end

to fade-links [ fade-percentage ]  ;; link method
  let link-expiration-age 10
  ;; once transparency is in town, this approach could get the heave-ho
  let links-to-kill links with [ age = link-expiration-age ]
  ask links-to-kill [ die ]  ; could kill associated parent turtles here since they're unnecessary
  let links-to-fade links with [ age != link-expiration-age ]
  fade-links-core links-to-fade fade-percentage
end

to fade-links-core [ link-agentset fade-percentage ]
  ask link-agentset
  ;; fade to black isn't great when background is variable
  ;; fade to background average would be superior
  [ let bgd-color link-background-color-ave
    let cur-color color
    let new-color rgb->rgb cur-color bgd-color fade-percentage
    set color new-color
  ]
    ;; FADE-TO-BLACK
    ;;let new-color ( color - ( 8 * fade-percentage / 100 )  )
    ;;ifelse (shade-of? color new-color)
    ;;[ set color new-color ]
    ;;[ set color black ]  ; could also revert to landscape background
end

to fade-patches [ fade-percentage ]  ;; patch method for 'path' patches
  let path-patches patches with [ path = 1 ]    ;; otherwise, landscape patches are altered
  let patches-to-kill path-patches with [ pcolor = black ]
  ask patches-to-kill [ color-patch-using-fitness pxcor pycor ]
  let patches-to-fade path-patches with [ pcolor != black ]
  fade-patches-core patches-to-fade fade-percentage
end

to fade-patches-core [ patch-agentset fade-percentage ]
  ask patch-agentset
  [ let new-color ( pcolor - ( 8 * fade-percentage / 100 )  )
    ifelse (shade-of? pcolor new-color)
    [ set pcolor new-color ]
    [ set pcolor black ]  ; could also revert to landscape background
    ]
end

;;
;; ===== Chromosomes
;;
to fixed-chromosome-defaults
  ;; number of mutable positions in chromosome (number of 'residues' in 'polymer')
  set chromosome-length
      ;; CHROMOSOME
      ;;10 ;; bit encoding with two 5-bit words
      2  ;; float encoding with two float words
  ;; maximum value a chromosome can have at a given position
  set chromosome-pos-max
      ;; CHROMOSOME
      ;;1  ;; bit encoding
      4.5  ;; float encoding
  ;; minimum value a chromosome can have at a given position
  set chromosome-pos-min
      ;; CHROMOSOME
      ;;0  ;; bit encoding
      -5.5  ;; float encoding
  ;; word length: number of residues in a word (if residues are grouped into 'words')
  set word-length
  ;; CHROMOSOME
  ;; bit encoding (two 5-bit words): 5
  ;; float encoding:
  1
  ;; if a word can correspond to a numeric value... these are the corresponding
  ;; floating point values
  ;; (netlogo only supports floats - no binary encodings, etc.)
  set word-max 4.5        ;; CHROMOSOME
  set word-min -5.5       ;; CHROMOSOME
  set word-range          ;; CHROMOSOME
      ( word-max - word-min )
  ;; range of values possible at a given residue
  ;; - only relevant with numeric values
  ;; - set to false for non-numeric values
  set residue-range 		;; CHROMOSOME
      (chromosome-pos-max - chromosome-pos-min)
end

;; return nth word in chromosome (numbering begins with 1)
to-report chromosome-word [ some-chromosome n ]
  ;;let index2 ( word-length * n )
  ;;let index1 ( index2 - word-length )
  report sublist some-chromosome  ( word-length * ( n - 1 ) ) ( word-length * n )
end

to test-chromosome-word
  print chromosome-word [ 1 1 1 1 1 0 0 0 0 0 ] 1
end

to create-new-chromosome-float
  let x1 ( -5.5 + random 11 )
  let x2 ( -5.5 + random 11 )
  set chromosome list x1 x2
end

;; suitable for display
to-report sample-chromosome
  let some-turtle one-of turtles
  let x 0
  ask some-turtle
    [ ;; CHROMOSOME
      ;; float:
      set x word precision item 0 chromosome 1 word " " precision item 1 chromosome 1
      ;; bit encoding: set x chromosome
    ]
  report x
end

;;
;; ===== Fitness
;;

;; set MAX-OBSERVED-FITNESS if current generation has fitness values available
to set-max-observed-fitness
  let this-generation-agentset turtles with [ generation = generations ]
  if ( any? this-generation-agentset )
  [ let this-generation-max max [fitness] of this-generation-agentset
    if ( this-generation-max > max-observed-fitness )
    [ set max-observed-fitness this-generation-max ]
   ]
end

to-report pprint-current-max-fitness
  report precision max [ fitness ] of turtles 1
end

to fixed-fitness-defaults
  ;; real values representing theoretical minimum and maximum fitness values possible
  set min-fitness
    ;; FITNESS
    ;; L2a: 0
    -1 ;; L2c
  set max-fitness
    ;; FITNESS
    ;; L2a: fitness-of-chromosome-float (list -5.5 -5.5)
    1
  set fitness-range ( max-fitness - min-fitness )
  set max-observed-fitness min-fitness
end

to calculate-fitness-of-turtles
  ask turtles [ calculate-fitness-of-turtle ]
end

to calculate-fitness-of-turtle       ;; turtle procedure
  ;; single chromosome assumption
  set fitness fitness-of-chromosome chromosome
end

to-report fitness-of-chromosome [ some-chromosome ]
  ;; FITNESS-OF-CHROMOSOME-FOO
  ;; - defines fitness function for encoding FOO
  report
  ;; fitness-of-chromosome-bit    ; bit encoding
  fitness-of-chromosome-float  ; float encoding
  some-chromosome
end

to-report fitness-of-chromosome-float [ some-chromosome ]
  ;; FITNESS
  let x1 item 0 some-chromosome
  let x2 item 1 some-chromosome
  ;; definition of fitness function
  ;; - function with parameters x1, x2, ... xN where each corresponds to a position in the chromosome
  ;; L2a: report ( x1 ^ 2 + x2 ^ 2 )
  ;; L2c: report ( ( sin ( 0.16 * radians->degrees x1 ) ) * ( sin ( 0.16 * radians->degrees x2 ) ) )

  ;; L3: report ( ( sin ( 0.56 * radians->degrees x1 ) ) * ( sin ( 0.56 * radians->degrees x2 ) ) )
  ;; L4:
  report ( ( sin ( 1.2 * radians->degrees x1 ) ) * ( sin ( 1.2 * radians->degrees x2 ) ) )
end

to fitness-of-chromosome-float-test
    type "[ -5.5 -5.5 ] fitness: " print fitness-of-chromosome-float [ -5.5 -5.5 ]
    type "[ 0 0 ] fitness: " print fitness-of-chromosome-float [ 0 0 ]
end

to color-using-fitness
  let new-color scale-color red fitness max-fitness practical-fitness-min
  set color new-color
end

;;
;; ===== Mutations
;;
to mutate-turtle   ;; turtle procedure
  ;; single chromosome
  mutate-turtle-chromosome
end

to mutate-turtle-test
  ask turtles [ die ]
  create-turtle-population
  show [ chromosome ] of turtles with [ id = 1 ]
  repeat 6
  [ ask turtles with [ id = 1 ] [ mutate-turtle ]
    print [ chromosome ] of turtles with [ id = 1 ]
  ]
end

to mutate-turtle-chromosome
   set chromosome mutate-chromosome-reporter chromosome
end

to-report mutate-chromosome-reporter [ some-chromosome ]
  let x 0
  ;; mutate residue-by-residue with probability RESIDUE-MUTATION-RATE
  repeat chromosome-length
  [ let value item x some-chromosome
    if ( random-float 100.0 < residue-mutation-rate )
    [ 
      let tentative-value point-mutate value
      let tentative-chromosome replace-item x some-chromosome tentative-value
      ;; enforce range restriction(s) at word level
      if ( chromosome-words-in-range-p tentative-chromosome )
      [ set some-chromosome tentative-chromosome ]
     ]
     set x ( 1 + x )
   ]
   report some-chromosome
end

to test-mutate-chromosome-reporter
  let new-chromosome mutate-chromosome-reporter
  ;; bit test: [ 0 0 0 0 0 0 0 0 0 0 ]
  ;; float test:
  [ 3 3 ]
  show new-chromosome
end

;; check if words in SOME-CHROMOSOME are in range specified by WORD-MAX and WORD-MIN
to-report chromosome-words-in-range-p [ some-chromosome ]
  let words-in-chromosome ( chromosome-length / word-length )
  let n 1
  ;;let flag true
  repeat words-in-chromosome
  [ let nth-word chromosome-word some-chromosome n
    ;; netlogo math is in floating point (no binary encoding, etc.)
    let nth-word-float
    	;; CHROMOSOME
	    ;; bit encoding: bits->number nth-word 1 true
	    ;; float encoding:
	    item 0 nth-word
    if ( not in-range nth-word-float word-min word-max )
    [ report false ]
    set n ( 1 + n )
   ]
   report true
end

to test-chromosome-words-in-range-p
  ;; bit encoding (5-bit words, L2a landscape)
  type "should return true: "  print chromosome-words-in-range-p [ 0 1 0 1 1 0 1 0 1 1 ]
  type "should return false: "  print chromosome-words-in-range-p [ 1 1 1 1 1 1 1 0 1 1 ]
  type "should return false: "  print chromosome-words-in-range-p [ 1 1 1 1 1 1 1 1 1 1 ]
  type "should return false: "  print chromosome-words-in-range-p [ 0 1 1 1 1 0 1 1 1 1 ]
  type "should return false: "  print chromosome-words-in-range-p [ 0 1 0 1 1 1 1 1 1 1 ]
end

;; specification of the residue mutation operator
to-report point-mutate [ value ]
  ;; mutation operator:
  ;; CHROMOSOME
  ;; float encoding:
  let tentative mutate-locus-gaussian value
  ;; bit encoding:  let tentative mutate-bit value
  ifelse ( in-range tentative chromosome-pos-min chromosome-pos-max )
  [ report tentative ]
  [ report value ]
end

to point-mutate-test
  ;; with bit encoding
  print point-mutate 0
  print point-mutate 1
end

;;
;; ==== Point mutation operators
;;

;; Gaussian_mutate: mutates a given locus by incrementing/decrementing
;; the value by a Gaussian delta.
to-report mutate-locus-gaussian [ locus-value ]
    let new-val ( locus-value + ( 1.3 * mut-delta * rand-gaussian ) )
    report new-val
end

to mutate-locus-gaussian-test
  repeat 4
    [ show mutate-locus-gaussian 0 ]
end

;; ====== Plotting

to setup-plotting
  ;; x interval per plot event:
  ;; 1: plot with each birth
  ;; generation-size: plot with each generation
  let x-pen-interval population-size   ; fixed population size
  set-current-plot "Fitness"
  set-current-plot-pen "avg"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "max"
  set-plot-pen-interval x-pen-interval
  set-current-plot-pen "min"
  set-plot-pen-interval x-pen-interval
end

to do-plotting
  let fitness-list [ fitness ] of turtles with [ deleted-p = false ]
  let best-fitness max fitness-list
  let avg-fitness mean fitness-list
  let worst-fitness min fitness-list
  set-current-plot "Fitness"
  set-current-plot-pen "avg"
  plot avg-fitness
  set-current-plot-pen "max"
  plot best-fitness
  set-current-plot-pen "min"
  plot worst-fitness
end

;;
;; ==== Utilities
;;

;; 1 radian = 180/pi degrees
to-report radians->degrees [ some-radians ]
  report ( 180 * some-radians / pi )
end

to-report greatest-2-divisor [ number ]
  let n 0
  loop
  [ ifelse ( ( number / 2 ^ n ) >= 1 )
    [ set n ( 1 + n ) ]
    [ set n ( n - 1 )
      report n ]
   ]
end

to greatest-2-divisor-test
  type "test with 4: " print greatest-2-divisor 4
  type "test with 5: " print greatest-2-divisor 5
  type "test with 1: " print greatest-2-divisor 1
  type "test with 43: " print greatest-2-divisor 43
  type "test with 32: " print greatest-2-divisor 32
end

;; Box-Muller (1958) transformation
;; uniformly distributed random variables ->  set of random variables with Gaussian distribution
;; polar form:

to-report rand-gaussian
    let w 0
    let x1 0
    let x2 0
    loop
    [
    ;; random floats between 0 and 1
    let r1 ( random 10 / 9 )
    let r2 ( random 10 / 9 )
    set x1 ( 2.0 * r1 - 1.0 )
    set x2 ( 2.0 * r2 - 1.0 )
    set w ( x1 * x1 + x2 * x2 )
    if ( w < 1.0 ) [
       set w sqrt ( (-2.0 * ln( w ) ) / w )
       report ( x1 * w )   ;; y1
       ;; could also return second random gaussian:     y2 = x2 * w;
     ]
    ]
end

;; return true if N is within range defined by A (the lesser value of A and B) and B (inclusive)
to-report in-range [ x a b ]
  report ( ( x >= a ) and ( x <= b ) )
end

;;
;; color management utilities
;;

;; blend rgb1 (list of three integers, each 0-255) toward rgb2 by percentage (0 - 100)
to-report rgb->rgb [ rgb1 rgb2 percentage ]
  let r11 item 0 rgb1
  let r12 item 1 rgb1
  let r13 item 2 rgb1
  let r21 item 0 rgb2
  let r22 item 1 rgb2
  let r23 item 2 rgb2
  let final1 ( r11 + ( ( percentage / 100 ) * ( r21 - r11 ) ) )
  let final2 ( r12 + ( ( percentage / 100 ) *  ( r22 - r12 ) ) )
  let final3 ( r13 + ( ( percentage / 100 ) *  ( r23 - r13 ) ) )
  report (list final1 final2 final3 )
end


;; link method - return RGB average corresponding to patch colors 'under' link
to-report link-background-color-ave
  report turtles-background-color-ave end1 end2
end

;; example:
;; observer> ask link 8 42 [ print link-background-color-ave ]
;; [118 135 144]

to-report turtles-background-color-ave [ turtle1 turtle2 ]
  let x1 [ xcor ] of turtle1
  let y1 [ ycor ] of turtle1
  let x2 [ xcor ] of turtle2
  let y2 [ ycor ] of turtle2
  let color-ave patch-color-ave x1 y1 x2 y2
  report color-ave
end

to-report patch-color-ave [ x1 y1 x2 y2 ]
  let rgb-color1 extract-rgb [ pcolor ] of patch x1 y1 ; patch colors are netlogo colors
  let rgb-color2 extract-rgb [ pcolor ] of patch x2 y2
  report rgb-ave rgb-color1 rgb-color2
end


;; return 'average' of rgb colors RGB1 and RGB2
to-report rgb-ave [ rgb1 rgb2 ]
  report rgb->rgb rgb1 rgb2 50
end

@#$#@#$#@
GRAPHICS-WINDOW
319
10
672
384
-1
-1
5.2
1
10
1
1
1
0
1
1
1
0
65
0
65
0
0
1
ticks
30.0

BUTTON
70
207
125
240
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
10
169
65
202
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
11
247
182
280
population-size
population-size
5
200
15
2
1
NIL
HORIZONTAL

PLOT
8
10
288
160
Fitness
births
raw fitness
0.0
20.0
0.0
1.0
true
true
"" ""
PENS
"max" 1.0 0 -2674135 true "" ""
"avg" 1.0 0 -10899396 true "" ""
"min" 1.0 0 -13345367 true "" ""

SLIDER
11
286
183
319
max-births
max-births
0
4000
4000
50
1
NIL
HORIZONTAL

SLIDER
11
324
183
357
residue-mutation-rate
residue-mutation-rate
0
100
50
5
1
NIL
HORIZONTAL

BUTTON
70
169
125
202
NIL
defaults
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
11
370
240
415
sample chromosome
sample-chromosome
17
1
11

MONITOR
180
164
287
209
maximum fitness
pprint-current-max-fitness
17
1
11

BUTTON
11
207
66
240
step
step
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
184
286
304
319
visualize-p
visualize-p
0
1
-1000

SLIDER
185
324
304
357
mut-delta
mut-delta
0
2
1
0.2
1
NIL
HORIZONTAL

MONITOR
197
215
286
264
turtle count
count turtles with [ deleted-p = false ]
17
1
12

@#$#@#$#@
## HOW TO USE IT

Press the SETUP button to create an initial population of organisms.

Press the STEP button to proceed one time through the algorithm steps, stopping at the point where the parent generation and the offspring generation can both be visualized (see VISUALIZE-P for more on visualization).

Press the GO button to cycle repeatedly through the algorithm.

Each organism is visualized as a 'turtle'. The position of the turtle on the 'fitness landscape' corresponds to the fitness of the turtle (lighter shades of blue on the fitness landscape correspond to higher fitness values).

The "Fitness Plot" depicts the highest, average, and minimum fitness values of the organisms as the model progresses.

The MUTATION-RATE slider controls the percent chance of mutation.

The POPULATION-SIZE slider controls the number of organisms present in the population.

If VISUALIZE-P is true, each turtle leaves a 'trace' between itself and its offspring as it produces one or more child organisms. Traces fade to the background color as time passes. Thus, one can visualize the consequence of each mutation event (modifying the child genome relative to the parent genome) as it occurs. Additionally, if VISUALIZE-P is true, at the time of generation of a filial population, organisms in the previous generation (parents) are visually accentuated (in a lighter shade of blue) and the organisms in the new generation (children) are visually accentuated (organism size is larger and organism shape is a 'turtle' shape). The corresponding fitness values are shown.

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.3.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
